# README #

#make
#./warmup1 sort [file]

### What is this repository for? ###

Sorts the bank transactions based on timestamp.

### Description ###

### First Part: ###
	Implementing Doubly Linked List
    
![Pic1](http://merlot.usc.edu/cs402-s18/projects/warmup1/list.gif)

![Pic2](http://merlot.usc.edu/cs402-s18/projects/warmup1/my402list.gif)
    
    
### int Length() ###
Returns the number of elements in the list.

### int Empty() ###
Returns TRUE if the list is empty. Returns FALSE otherwise.

### int Append(void *obj) ###
If list is empty, just add obj to the list. Otherwise, add obj after Last(). This function returns TRUE if the operation is performed successfully and returns FALSE otherwise.

### int Prepend(void *obj) ###
If list is empty, just add obj to the list. Otherwise, add obj before First(). This function returns TRUE if the operation is performed successfully and returns FALSE otherwise.

### void Unlink(My402ListElem *elem) ###
Unlink and delete elem from the list. Please do not delete the object pointed to by elem and do not check if elem is on the list.

### void UnlinkAll() ###
Unlink and delete all elements from the list and make the list empty. Please do not delete the objects pointed to by the list elements.

### int InsertBefore(void *obj, My402ListElem *elem) ###
Insert obj between elem and elem->prev. If elem is NULL, then this is the same as Prepend(). This function returns TRUE if the operation is performed successfully and returns FALSE otherwise. Please do not check if elem is on the list.

### int InsertAfter(void *obj, My402ListElem *elem) ###
Insert obj between elem and elem->next. If elem is NULL, then this is the same as Append(). This function returns TRUE if the operation is performed successfully and returns FALSE otherwise. Please do not check if elem is on the list.

### My402ListElem *First() ###
Returns the first list element or NULL if the list is empty.

### My402ListElem *Last() ###
Returns the last list element or NULL if the list is empty.

### My402ListElem *Next(My402ListElem *elem) ###
Returns elem->next or NULL if elem is the last item on the list. Please do not check if elem is on the list.

### My402ListElem *Prev(My402ListElem *elem) ###
Returns elem->prev or NULL if elem is the first item on the list. Please do not check if elem is on the list.

### My402ListElem *Find(void *obj) ###
Returns the list element elem such that elem->obj == obj. Returns NULL if no such element can be found.

### int Init() ###
Initialize the list into an empty list. Returns TRUE if all is well and returns FALSE if there is an error initializing the list.


### Second Part: ###
   Implementing sort functionality using above funtions.
   	warmup1 sort [tfile]
Square bracketed items are optional. If tfile is not specified, your program should read from stdin. Unless otherwise specified, output of your program must go to stdout and error messages must go to stderr.
The meaning of the commands are:

sort	  :  	Produce a sorted transaction history for the transaction records in tfile (or stdin) and compute balances. The input file should be in the tfile format.
The output for various commands are as follows.


    +-----------------+--------------------------+----------------+----------------+
    |       Date      | Description              |         Amount |        Balance |
    +-----------------+--------------------------+----------------+----------------+
    | Thu Aug 21 2008 | ...                      |      1,723.00  |      1,723.00  |
    | Wed Dec 31 2008 | ...                      | (       45.33) |      1,677.67  |
    | Mon Jul 13 2009 | ...                      |     10,388.07  |     12,065.74  |
    | Sun Jan 10 2010 | ...                      | (      654.32) |     11,411.42  |
    +-----------------+--------------------------+----------------+----------------+




### Who do I talk to? ###

Sandesh Vishwanath