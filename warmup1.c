#include <stdio.h>
#include <stdlib.h>
#include "my402list.h"
#include <string.h>
#include <time.h>
#include <locale.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct dataType{
	char transType;
	long time;
	long amount;
	char transDescr[100];
}dataStr;

int checkValue(char* s)
{
	int i=0;
	for(i=0;i<strlen(s);i++)
	{
		if(s[i]<'0' || s[i]>'9')
			return 0;
	}
	return 1;
}

void processTime(dataStr *ob,char* s)
{
	if(checkValue(s)!=0)
	{
		long timer=atol(s);
		time_t t;
		if(strlen(s)>=11 || timer<0 || timer>time(&t) || timer>4294967295u)
		{
			fprintf(stderr,"%s","Bad timestamp\n" );
			exit(1);
		}
		ob->time=timer;
	}
	else
		{
			fprintf(stderr,"%s","Wrong format of timestamp\n");
		}
}

void processAmt(dataStr* ob,char* s)
{
	
	int i=0,cnt=0,cntDec=-1;
	for(i=0;i<strlen(s);i++)
	{
		if((s[i]>='0' && s[i]<='9') || s[i]=='.')
		{
			if(s[i]!='.' && cnt!=-1)
				cnt++;
			else
			{
				if(cnt>7)
				{
					fprintf(stderr,"%s","Amount exceeds maximum limit\n");
					exit(1);
				}
				else
					cnt=-1;
			}
			if(cnt==-1)
				cntDec++;
		}
		else
		{
			fprintf(stderr,"%s","Invalid amount\n");
			exit(1);
		}
	}
	if(cntDec>2 || cnt==0)
	{
		fprintf(stderr,"%s","Not valid amount\n");
		exit(1);
	}
	double amt=atof(s)*100;
	ob->amount=(long)amt;
}
	
void trim(char *s)
{
	if(s==0L)
	{
		fprintf(stderr, "%s\n","Empty string" );
		exit(1);
	}
	char *p=s;
	int len=strlen(p);
	while(*p && isspace(*p))
		{
			++p;
			--len;
		}
	memmove(s,p,len+1);
}

void processDescr(dataStr* obj,char *s)
{
	trim(s);
	if(strlen(s)==0)
	{
		fprintf(stderr,"%s","Description cannot be empty\n");
		exit(1);
	}
	size_t l=strlen(s)-1;
	if(*s && s[l]=='\n')
		s[l]='\0';
	strncpy(obj->transDescr,s,strlen(s));
}

void printList(My402List* list)
{
	dataStr* tmp=0L;
	My402ListElem *elem=My402ListLast(list);
	if(list->num_members==0)
	{
		printf("List is empty\n");
		return;
	}
	while(elem!=0L)
	{
		tmp=(dataStr*)elem->obj;
		printf("%c\t",(tmp->transType));	
		printf("%ld\t",tmp->time );
		printf("%ld\t",tmp->amount );
		printf("%s\n",tmp->transDescr );
		elem=My402ListNext(list,elem);
	}
	printf("\n");
}

int insert(My402List* list,dataStr* object)
{
	My402ListElem* first=My402ListFirst(list);
	long time=object->time;
	while(first!=0L)
	{
		dataStr* tmp=(dataStr*)first->obj;
		if(time==tmp->time)
		{
			fprintf(stderr, "%s\n", "Another object has same timestamp");
			exit(1);
		}
		first=My402ListNext(list,first);
	}
	return My402ListAppend(list,object);
}

void processLine(char* line,My402List *list)
{
	int cnt=0,j=0;
	for(j=0;j<strlen(line);j++)
	{
		if(line[j]=='\t')
			cnt++;
	}
	if(cnt!=3)
	{
		fprintf(stderr,"%s","Invalid format of the line\n");
		exit(1);
	}
	dataStr* object=malloc(sizeof(dataStr));
	char str[5][1025];
	memset(str,'\0',sizeof(str[0][0])*5*1025);
	char *pch;
	pch=strtok(line,"\t");
	int i=0;
	while(pch!=0L)
	{
		strncpy(str[i],pch,strlen(pch));
		pch=strtok(0L,"\t");
		i++;
	}
	if(str[0][0]!='+' && str[0][0]!='-')
	{
		fprintf(stderr, "%s\n", "Transaction type is invalid");
		exit(1);
	}
	object->transType=str[0][0];
	processTime(object,str[1]);
	processAmt(object,str[2]);
	processDescr(object,str[3]);
	//printf("%c\n",object->transType);
	//printf("%ld\n",object->time );
	//printf("%ld\n",object->amount);
	//printf("%s\n",object->transDescr );
	if(insert(list,object)==0)
	{
		fprintf(stderr, "%s\n","could not append the object" );
	}
}

int readLine(FILE *fp,My402List *list)
{
	const int lineSize=1200;
	char line[lineSize];
	int lineCnt=0;
	while(fgets(line,lineSize,fp)!=0L)
	{
		if(strlen(line)>1024)
		{
			fprintf(stderr,"%s","line exceeds 1024 characters\n" );
			exit(1);
		}
		processLine(line,list);
		lineCnt++;
	}
	fclose(fp);
	return lineCnt;
}

void BubbleForward(My402List *pList, My402ListElem **pp_elem1, My402ListElem **pp_elem2)
    /* (*pp_elem1) must be closer to First() than (*pp_elem2) */
{
    My402ListElem *elem1=(*pp_elem1), *elem2=(*pp_elem2);
    void *obj1=elem1->obj, *obj2=elem2->obj;
    My402ListElem *elem1prev=My402ListPrev(pList, elem1);
/*  My402ListElem *elem1next=My402ListNext(pList, elem1); */
/*  My402ListElem *elem2prev=My402ListPrev(pList, elem2); */
    My402ListElem *elem2next=My402ListNext(pList, elem2);

    My402ListUnlink(pList, elem1);
    My402ListUnlink(pList, elem2);
    if (elem1prev == NULL) {
        (void)My402ListPrepend(pList, obj2);
        *pp_elem1 = My402ListFirst(pList);
    } else {
        (void)My402ListInsertAfter(pList, obj2, elem1prev);
        *pp_elem1 = My402ListNext(pList, elem1prev);
    }
    if (elem2next == NULL) {
        (void)My402ListAppend(pList, obj1);
        *pp_elem2 = My402ListLast(pList);
    } else {
        (void)My402ListInsertBefore(pList, obj1, elem2next);
        *pp_elem2 = My402ListPrev(pList, elem2next);
    }
}

void BubbleSortForwardList(My402List *pList, int num_items)
{
    My402ListElem *elem=NULL;
    int i=0;

    if (My402ListLength(pList) != num_items) {
        fprintf(stderr, "List length is not %1d in BubbleSortForwardList().\n", num_items);
        exit(1);
    }
    for (i=0; i < num_items; i++) {
        int j=0, something_swapped=FALSE;
        My402ListElem *next_elem=NULL;

        for (elem=My402ListFirst(pList), j=0; j < num_items-i-1; elem=next_elem, j++) {
        	dataStr* tmp1=(dataStr*)elem->obj;
            long cur_val=tmp1->time, next_val=0;

            next_elem=My402ListNext(pList, elem);
            dataStr* tmp2=(dataStr*)next_elem->obj;
            next_val = tmp2->time;

            if (cur_val > next_val) {
                BubbleForward(pList, &elem, &next_elem);
                something_swapped = TRUE;
            }
        }
        if (!something_swapped) break;
    }
}

void PrintTestList(My402List *pList, int num_items)
{
    My402ListElem *elem=NULL;

    if (My402ListLength(pList) != num_items) {
        fprintf(stderr, "List length is not %1d in PrintTestList().\n", num_items);
        exit(1);
    }
    for (elem=My402ListFirst(pList); elem != NULL; elem=My402ListNext(pList, elem)) {
        dataStr* tmp=(dataStr*)elem->obj;
        long ival=tmp->time;
        char* stri=tmp->transDescr;
        fprintf(stdout, "%ld\t", ival);
        fprintf(stdout, "%s\n", stri);
    }
    fprintf(stdout, "\n");
}

void sort(My402List* list,int lineCnt)
{
	//My402List *listTmp=list;
	BubbleSortForwardList(list,lineCnt);
	//PrintTestList(listTmp,lineCnt);
}

void printHeader()
{
	
    printf("%s\n","+-----------------+--------------------------+----------------+----------------+");
    printf("%s\n","|       Date      | Description              |         Amount |        Balance |");
    printf("%s\n","+-----------------+--------------------------+----------------+----------------+");
}

char* formatTime(long time)
{
	//time_t tr=time;
	return ctime(&time);
}

void formatDescr(char *str)
{
	int i=0;
	for(i=0;i<24;i++)
		{
			if(i<strlen(str))
				printf("%c",str[i] );
			else
				printf(" ");
		}
	printf(" ");
}

long formatAmt(long amt,char sign,long bal)
{
	double amtR=(double)amt/100;
	setlocale(LC_NUMERIC,"en_US");
	if(sign=='-')
	{
		bal=bal-amt;
		printf("| (%'12.2lf) |",amtR);
		if(bal>=0)
			printf("%'14.2lf  |",((double)bal)/100);
		else
			printf(" (%'12.2lf) |",(-1)*((double)bal)/100);
		return bal;
	}
	bal+=amt;
	printf("| %'13.2lf  |",amtR);
	if(bal>=0)
		printf("%'14.2lf  |",((double)bal)/100);
	else
		printf(" (%'12.2lf) |",(-1)*((double)bal)/100);
	return bal;
}

void printfooter()
{
	int i=0;
	for(i=0;i<80;i++)
	{
		if(i==0 || i==18 || i==45 || i==62 || i==79)
			printf("+");
		else
			printf("-");
	}
}

void formatter(My402List* list)
{
	My402ListElem *tmp=My402ListFirst(list);
	long balance=0;
	while(tmp!=0L)
	{
		dataStr* elem=(dataStr*)tmp->obj;
		char* str=formatTime(elem->time);
		char strCpy[15];
		strCpy[0]='\0';
		int i=0,j=0;
		for(i=0;i<strlen(str);i++)
		{
			if((i>=0 && i<10) || (i>=19 && i<24))
				strCpy[j++]=str[i];
		}
		printf("| %s | ",strCpy);
		//strCpy[15]='\0';
		formatDescr(elem->transDescr);
		balance=formatAmt(elem->amount,elem->transType,balance);
		printf("\n");
		tmp=My402ListNext(list,tmp);
	}
	printfooter();
}

int main(int argc,char** argv)
{
	FILE *fptr=0L;
	My402List *list=malloc(sizeof(My402List));
	int lineCnt=0;
	if(My402ListInit(list)==0)
	{
		fprintf(stderr,"%s","Memory could not be allocated\n" );
		exit(1);
	}
	if(argc<=1)
	{
		fprintf(stderr,"%s","Please enter a command\n");
		exit(1);
	}
	else
	{
		if(argc==2)
		{
			if((strncmp(argv[1],"sort",strlen(argv[1])))==0)
			{
				lineCnt=readLine(stdin,list);
			}
			else
			{
				fprintf(stderr,"%s","Please enter correct command\n");
				exit(1);
			}
		}
		else if(argc==3)
		{
			if((strncmp(argv[1],"sort",strlen(argv[1])))==0)
				{
					char *path=argv[2];
					struct stat statbuf;
					if(stat(path,&statbuf)==-1)
						{
							perror("Error:");
							exit(1);
						}
					if(S_ISDIR(statbuf.st_mode)!=0)
					{
						printf("%s\n","Path is a directory" );
						exit(1);
					}
					if(access(argv[2],"R_OK"))
					{
						perror("R_OK");
						exit(1);
					}
					
					fptr=fopen(argv[2],"r");
					if(fptr!=0L)
					{
						lineCnt=readLine(fptr,list);
					}
					else
					{
						//printf("%s - File does not exist",argv[2]);
						perror("Error:");
						exit(1);
					}
				}
		}
		else
			{
				fprintf(stderr,"%s","Too many arguments\n");
				exit(1);
			}
	}
	sort(list,lineCnt);
	printHeader();
	formatter(list);
	printf("\n");
	return 0;
}