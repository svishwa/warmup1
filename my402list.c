#include "my402list.h"
#include <stdlib.h>
#include <stdio.h>
My402ListElem *first=0L,*last=0L;
int My402ListInit(My402List* List)
{
	if(List==0L)
		return 0;
	List->num_members=0;
	List->anchor.obj=0L;
	List->anchor.next=0L;
	List->anchor.prev=0L;
	return 1;
}

int  My402ListLength(My402List* List)
{
	return List->num_members;
}

int  My402ListEmpty(My402List* List)
{
	return List->num_members<=0;
}

My402ListElem *My402ListFirst(My402List* List)
{
	if(List->num_members==0 || List==NULL)
		return 0L;
	return List->anchor.next;;
}

My402ListElem *My402ListLast(My402List* List)
{
	if(List->num_members==0 || List==NULL)
		return 0L;
	return List->anchor.prev;
}

int  My402ListAppend(My402List* List, void* ob)
{
	My402ListElem *elem=malloc(sizeof(My402ListElem));
	if(elem!=0L)
	{
		elem->obj=ob;
		My402ListElem *last=My402ListLast(List);
		if(last!=0L)
		{
			last->next=elem;
			elem->prev=last;
			elem->next=&(List->anchor);
			List->anchor.prev=elem;
		}
		else
		{
			elem->next=elem->prev=&(List->anchor);
			(List->anchor).next=(List->anchor).prev=elem;
		}
		List->num_members++;
		return 1;
	}
	return 0;
}

int  My402ListPrepend(My402List* List, void* ob)
{
	My402ListElem *elem=malloc(sizeof(My402ListElem));
	if(elem!=0L)
	{
		elem->obj=ob;
		My402ListElem *first=My402ListFirst(List);
		if(first!=0L)
		{
			first->prev=elem;
			elem->next=first;
			elem->prev=&(List->anchor);
			List->anchor.next=elem;
		}
		else
		{
			elem->next=elem->prev=&(List->anchor);
			(List->anchor).next=(List->anchor).prev=elem;
		}
		List->num_members++;
		return 1;
	}
	return 0;
}

void printlist(My402List* List)
{
	My402ListElem *elem=My402ListLast(List);
	if(List->num_members==0)
	{
		printf("List is empty\n");
		return;
	}
	while(elem!=0L)
	{
		printf("%d\t",*((int*)(elem->obj)));	
		elem=My402ListPrev(List,elem);
	}
	printf("\n");
}

My402ListElem *My402ListNext(My402List* List, My402ListElem* elem)
{
	if(elem!=0L)
	{
		My402ListElem *tmp=elem->next;
		if(My402ListLength(List)!=0 && tmp!=&(List->anchor))
			return tmp;
		return 0L;
	}
	return 0L;
}

My402ListElem *My402ListPrev(My402List* List, My402ListElem* elem)
{
	My402ListElem *tmp=elem->prev;
	if(tmp!=&(List->anchor))
		return tmp;
	return 0L;
}

My402ListElem *My402ListFind(My402List* List, void* ob)
{

	My402ListElem *elem=My402ListFirst(List);

	while(elem!=&(List->anchor) || elem!=0L )
	{
		if(elem->obj==ob)
			{
				return elem;
			}
		elem=My402ListNext(List,elem);
	}
	
	return 0L;
}

int  My402ListInsertAfter(My402List* List, void* ob, My402ListElem* elem)
{
	if(List->num_members==0 || elem==0L)
		return My402ListAppend(List,ob);
	My402ListElem* node=(My402ListElem* )malloc(sizeof(My402ListElem));
	if(node==0L)
		return 0;
	node->obj=ob;
	node->next=elem->next;
	elem->next->prev=node;
	node->prev=elem;
	elem->next=node;
	List->num_members++;
	return 1;
}

int  My402ListInsertBefore(My402List* List, void* ob, My402ListElem* elem)
{
	if(List->num_members==0 || elem==0L)
	{
		return My402ListPrepend(List,ob);
	}
	
	My402ListElem *node=(My402ListElem *)malloc(sizeof(My402ListElem));
	if(node==0L)
		return 0;
	node->obj=ob;
	node->prev=elem->prev;
	elem->prev->next=node;
	node->next=elem;
	elem->prev=node;
	List->num_members++;
	return 1;
}

void My402ListUnlink(My402List* List, My402ListElem* elem)
{
	if(elem==&(List->anchor) || elem==0L || List->num_members==0)
		return;
	if(List->num_members==1)
	{
		(List->anchor).prev=0L;
		(List->anchor).next=0L;
	}
	else{
	My402ListElem* tmpN=elem->next;
	My402ListElem* tmpP=elem->prev;
	tmpP->next=tmpN;
	tmpN->prev=tmpP;
	}
	free(elem);
	List->num_members--;
}

void My402ListUnlinkAll(My402List* List)
{
	if(List==0L)
		return;
	My402ListElem* tmp=&(List->anchor);
	while(List->num_members!=0)
	{
		My402ListUnlink(List,My402ListNext(List,tmp));
	}
	

}
